import { List, Record, fromJS } from 'immutable';
import { ILineup, IParty, PartyRecord } from './lineup.types';
import { INITIAL_STATE } from './lineup.initial-state';

import {
  PARTY_JOINED,
  PARTY_LEFT,
  PARTY_SEATED,
  PARTY_REORDERED
} from '../../constants';

export function lineupReducer(state: ILineup = INITIAL_STATE, action): ILineup {
  switch (action.type) {
    case PARTY_JOINED: return state.push(action.payload);
    case PARTY_LEFT: return state
      .filter(n => n.partyId !== action.payload.partyId) as ILineup;
    case PARTY_SEATED: return state
      .filter(n => n.partyId !== action.payload.partyId) as ILineup;
    case PARTY_REORDERED:
      //console.log("reordering parties!")
      return state.sort((a, b) => {

        var aIdx = action.payload.indexes.indexOf(a.partyId)

        var bIdx = action.payload.indexes.indexOf(b.partyId)

        return aIdx < bIdx ? -1 : 1;
      }) as ILineup;



    default:
      return state;
  }
};
