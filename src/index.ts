declare function require(name:string);

import 'reflect-metadata';
import 'babel-polyfill';
import 'core-js/es6';
import 'core-js/es7/reflect';
import '../shims/shims_for_IE';
import 'zone.js/dist/zone';
import 'ts-helpers';
import 'dragula';
import './polyfills'


import { enableProdMode, provide } from '@angular/core';
import { bootstrap } from '@angular/platform-browser-dynamic';
import { APP_BASE_HREF } from '@angular/common/index';
import { provideForms } from '@angular/forms';
import { DevToolsExtension, NgRedux } from 'ng2-redux';
import { Dragula,DragulaService } from 'ng2-dragula/ng2-dragula';

import { PartyService } from './services/party';
import { ACTION_PROVIDERS } from './actions';
import { HomePage } from './pages/home.page';


declare const __PRODUCTION__: boolean;
declare const __TEST__: boolean;
declare function require(name:string);

if (__PRODUCTION__) {
  enableProdMode();
} else {
  require('zone.js/dist/long-stack-trace-zone');
}

if (!__TEST__) {
  bootstrap(HomePage, [
    provideForms(),
    NgRedux,
    DevToolsExtension,
      Dragula,
    DragulaService,
    ACTION_PROVIDERS,
    PartyService,
    provide(APP_BASE_HREF, { useValue: '/' })
  ]);
}
