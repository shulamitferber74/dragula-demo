import {
  Component,
  Input,
  Output,
  ChangeDetectionStrategy,
  EventEmitter
} from '@angular/core';
import { REACTIVE_FORM_DIRECTIVES } from '@angular/forms';
import { ILineup } from '../../store';
import { Dragula, DragulaService } from 'ng2-dragula/ng2-dragula';

const TEMPLATE = require('./lineup.component.html');
@Component({
  selector: 'tb-lineup',
  template: TEMPLATE,
  changeDetection: ChangeDetectionStrategy.OnPush,
  directives: [REACTIVE_FORM_DIRECTIVES, Dragula],
  viewProviders: [DragulaService],
  styles: [`tr{background-color:#c6d8e1} .gu-mirror td{min-width:167px!important} .gu-mirror td:last-child{min-width:50px!important}`]
})
export class Lineup {

  @Input() lineup: ILineup;
  @Output() partyJoined: EventEmitter<any> = new EventEmitter();
  @Output() partyLeft: EventEmitter<any> = new EventEmitter();
  @Output() partyReordered: EventEmitter<any> = new EventEmitter();



  constructor(private dragulaService: DragulaService) {

    dragulaService.drag.subscribe((value) => {
      console.log(`drag: ${value[0]}`);
      this.onDrag(value.slice(1));
    });

    dragulaService.drop.subscribe((value) => {
      console.log(`drop: ${value[0]}`);
      this.onDrop(value.slice(1));
    });

  }

  private onDrag(args) {
    let [e, el] = args;
    // do something
  }

  private onDrop(args) {

    let [e, el] = args;

    var arr = el.getElementsByTagName('tr')

    var indexes = []

    for (var i = 0; i < arr.length; i++) {

      indexes.push(Number(arr[i].getAttribute('partyId')))
    }

    this.partyReordered.emit({ indexes })

  }


}
;
